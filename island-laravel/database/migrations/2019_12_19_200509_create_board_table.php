<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // if (Schema::hasTable('board')) {
       //   return;
       // }
        Schema::create('board', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
	        $table->tinyInteger('type');
            $table->string('bg_image')->nullable();
	        $table->string('bg_color')->nullable();
            $table->string('folder');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board');
    }
}
