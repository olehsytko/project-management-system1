<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_board', function (Blueprint $table) {
	    $table->bigInteger('id_user');
	    $table->bigInteger('id_board');
	    $table->timestamps();

	    $table->engine = 'InnoDB';
	    $table->charset = 'utf8';
	    $table->collation = 'utf8_general_ci';

	    $table->index('id_user');
	    $table->index('id_board');

	    $table->unique(['id_user', 'id_board']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
