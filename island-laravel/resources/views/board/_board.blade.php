<div class="col-md-3 min-board-panel">
    <a href="{{ url('/board', ['folder' => $board->folder, 'name' => $board->name]) }}" class="min-board" style="text-decoration:none;
        @if (!empty($board->bg_image))
            background-image: url('{{ $board->bg_image }}');
            background-size: cover;
        @elseif ($board->bg_color)
            background-color: {{ $board->bg_color }};
        @endif">
        <span class="board-title-fade"></span>
        <div style="position: relative;padding-top: 5px;padding-left: 10px;">{{ $board->name }}</div>
    </a>
</div>
