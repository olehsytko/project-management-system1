$(function() {
    initWorkflowSortable();
    initCardSortable();

    $(".alert").fadeTo(5000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

    $("body").mousedown(function(e){
        if ($(e.target).parents(".window-overlay").length === 0) {
            $('body').removeClass('window-up');
            $('.window-overlay').hide();
        }

        if ($(e.target).parents(".workflow-menu").length === 0) {
            $(".list-menu").hide();
            $(".list-menu").removeClass('active');
        }

        if ($(e.target).parents(".add-card").length === 0) {
            $(".add-card").hide();
            $(".btn-add-card").show();
        }

        if ($(e.target).parents("#invite-users").length === 0) {
            $("#invite-users").hide();
        }

        if ($(e.target).parents("#users").length === 0) {
            $("#users").hide();
        }

        if (!$(e.target).hasClass('label-card-title')) {
            $('.label-card-title').addClass('card-title-normal');
            $('.js-editing-target').removeClass('is-hidden');
        }
    });

    $('.create-board').on('click', function (e) {
        e.preventDefault();
        $('#modalNewBoard').modal('show');
    });

    $('#workflow-panel').on('click', '.btn-add-card', function () {
        var addCardForm = $(this).parent().children()[1];
        $(addCardForm).show();
        $($($(addCardForm).children()[0]).children()[1]).focus();
        $(this).hide();
    });

    $('#workflow-panel').on('click', '.close-add-card-form', function () {
        var addCardForm = $(this).parent().parent();
        $(addCardForm).hide();
        $('div.btn-add-card[data-id="' + $(this).attr('data-id') + '"]').show();
    });


    $(".main-panel").on('keypress', '.workflow-title', function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            event.preventDefault();
            addWorkflow();
        }
    });

    $('.main-panel').on('click', '#btn-add-list', function(e) {
        addWorkflow();
    });

    $("#workflow-panel").on('click', '.submit-add-card', function(e) {
        addCard($(this));
    });

    $("#workflow-panel").on('keypress', '.text-card', function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            event.preventDefault();
            addCard($(this));
        }
    });


    $("#workflow-panel").on('keyup', '.label-card-title', function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var title = $(this);
        if(keycode == '13'){
            event.preventDefault();
            title.addClass('card-title-normal');
            title.blur();
        }
    });

    $("#workflow-panel").on('blur', '.label-card-title', function() {
        renameList($(this));
    });

	$('#workflow-panel').on('click', '.js-editing-target', function(){
        $('.js-editing-target').removeClass('is-hidden');
        $('.label-card-title').addClass('card-title-normal');
		$(this).addClass('is-hidden');
		var cardTitle = $($(this).parent().children()[1]);
		cardTitle.removeClass('card-title-normal');
		cardTitle.focus();
	});

    $("#workflow-panel").on('click', '.js-close-window', function(e) {
        $('body').removeClass('window-up');
        $('.window-overlay').hide();
    });


    $('#workflow-panel').on('click', '.btn-list-menu', function(e) {
        $(".list-menu").hide();
        var menu = $(this).parent();
        var listMenu = $(menu.children()[1]);
        if (listMenu.hasClass('active')) {
            listMenu.hide();
            listMenu.removeClass('active');
        }
        else {
            $(".list-menu").removeClass('active');
            listMenu.show();
            listMenu.addClass('active');
        }

    });

    $('.background-grid-trigger').on('click', function(){
        if ( !$(this).hasClass('selected')) {
            $('.background-grid-trigger').removeClass('selected');
            $(this).addClass('selected');
        }
    })

    $('#create-board-form').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var bgImageUrl = $('.background-grid-trigger.is-photo.selected').attr('data-full') == undefined ? '' : $('.background-grid-trigger.selected').attr('data-full')
        var bgColor = $('.background-grid-trigger.is-color.selected').css('backgroundColor') == undefined ? '' : $('.background-grid-trigger.selected').css('backgroundColor');
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize() + "&bgImage=" + bgImageUrl + "&bgColor=" + bgColor,
            success: function(response)
            {
                window.location.href = response.url;
            }
        });
    })

    $(document).on('click', '#board-header-btn-invite', function(e){
        e.preventDefault();
        $('#invite-users').show();
    });

    $(document).on('click', '#btn-close-invite', function(e){
        e.preventDefault();
        $('#invite-users').hide();
    });

    $(document).on('click', '#board-header-btn-users', function(e){
        e.preventDefault();
        $('#users').show();
    });

    $(document).on('click', '#btn-close-users', function(e){
        e.preventDefault();
        $('#users').hide();
    });

    $(document).on('submit', '#invite-user-form', function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response)
            {
               // alert(response);
               //window.location.href = response.url;
            }
        });
    });

    $(document).on('click', '.dialog-close-button', function(e){
        $('body').removeClass('window-up');
        $('.window-overlay').hide();
    });

    $(document).on('click', '.min-card', function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: $(this).attr("href"),
            data: {
                id: $(this).attr('data-id'),
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                if (response.success) {
                    $('#modalChangeCard').html(response.html);
                    $('body').addClass('window-up');
                    $('.window-overlay').show();
                } else {
                    console.log(response);
                }
            }
        });
    });
});

function initWorkflowSortable(){
    $( "#workflow-panel" ).sortable({
        items: "div.list",
        cursor: 'move',
        appendTo: 'body',
        zIndex: 1100,
        update: function() {
            sendWorkflowOrder();
        }
    });
}

function initCardSortable(){
    $(".cards").sortable({
        items: ".min-card",
        connectWith: ".cards",
        containment: 'body',
        cursor: 'move',
        zIndex: 1100,
        update: function(event, ui) {
            sendCartsOrder(ui.item.parent().attr('data-workflowId'));
        },
        receive: function(ev, ui) {
            ui.item.parent().find('.cards').append(ui.item);
        }
    });
}

function addWorkflow() {
    var form = $('#add-list-form');
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        success: function(workflowView)
        {
            if (workflowView.success) {
                $(workflowView.html).appendTo($('#workflow-panel'));
                form[0].reset();
                initCardSortable();
            }
        }
    });
}

function addCard(object){
    var cardsPanel = object.parent().parent().parent().children()[0];
    var form = $('#add-card-form-' + object.attr('data-id'));
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        success: function(data)
        {
            if (data.success) {
                $(data.html).appendTo(cardsPanel);
                form[0].reset();
                $(form.children()[1]).focus();
            }
        }
    });
}

function renameList(title){
    $.ajax({
        type: "POST",
        url: renameListUrl,
        data: {
            id: $(title.parent().parent()).data('id'),
            name: title.val(),
            _token: $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response)
        {
        }
    });
}

function sendWorkflowOrder() {
    var order = [];
    $('div.list').each(function(index,element) {
        order.push({
            id: $(this).attr('data-id'),
            position: index+1
        });
    });

    $.ajax({
        type: "POST",
        dataType: "json",
        url: sortWorkflowUrl,
        data: {
            order:order,
            _token: $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
            if (response.status == "success") {
                console.log(response);
            } else {
                console.log(response);
            }
        }
    });
}

function sendCartsOrder(workflowId) {
    var order = [];
    $('div.workflow-' + workflowId + ' > .min-card').each(function(index,element) {
        order.push({
            id: $(this).attr('data-id'),
            position: index+1
        });
    });

    $.ajax({
        type: "POST",
        dataType: "json",
        url: sortCardUrl,
        data: {
            order:order,
            workflowId:workflowId,
            _token: $('meta[name="csrf-token"]').attr('content')
        },
        error: function(response) {
            if (response.status == "success") {
                console.log(response);
            } else {
                console.log(response);
            }
        }
    });
}
